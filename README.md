# Movie Finder App - (https://vercel.com/nigarumovum/movie-finder-react) 
## Built with: 
![React](https://img.icons8.com/plasticine/48/000000/react.png)
![css3](https://img.icons8.com/color/48/000000/css3.png)
![sass](https://img.icons8.com/color/48/000000/sass.png)

## Description:
Simple Web App to fetch MovieDB API to fetch data an return in with React UI Components

## Deployed in: Vercel

# About Me:

- 🤔 I’m looking for better oportunities and projects to growth my skills and gain experience.
- ⚡ Fun fact: I play video games, play the guitar/sing and learn new technologies very often.
- 🔭 My Portfolio:https://brealy-padron-portfolio-react.vercel.app/


:mailbox: Reach me out!
- 📫 How to reach me: neighbordevcr@gmail.com

<p align = "center">

[<img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" />](https://www.linkedin.com/in/bfpr131095/)

</p>

</hr>

![visitors](https://visitor-badge.glitch.me/badge?page_id=nigarumovum.nigarumovum)
